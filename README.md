# OpenML dataset: LottoMaster-144

https://www.openml.org/d/43067

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This is an experimental data set for trying to classify numbers in a lottery as &quot;Highly likely to be picked&quot; or &quot;Not very likely to be picked&quot;.  It is based on a little more than a years worth of actual daily drawing results from the Florida Fantasy Five, with derivative elements such as count of times picked, average times picked, days since last picked, etc.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43067) of an [OpenML dataset](https://www.openml.org/d/43067). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43067/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43067/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43067/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

